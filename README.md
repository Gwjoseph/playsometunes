# playsometunes 🤖 #

Discord bot to play some tunes.

### Dependencies ###

* node v15 or above
* youtube-dl command line package

Run `npm -i` to install JavaScript package dependencies.
