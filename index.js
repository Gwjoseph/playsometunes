const { Client, Intents } = require('discord.js');
const {
	NoSubscriberBehavior,
	StreamType,
	createAudioPlayer,
	createAudioResource,
	entersState,
	AudioPlayerStatus,
	VoiceConnectionStatus,
	joinVoiceChannel,
} = require('@discordjs/voice');
const { exec } = require("child_process");
const fs = require('fs');
const { token } = require('./config.json');

const cacheDir = './downloaded_audio';

if (!fs.existsSync(cacheDir)){
	fs.mkdirSync(cacheDir);
}

const client = new Client({ intents: [
	Intents.FLAGS.GUILDS,
	Intents.FLAGS.GUILD_MESSAGES,
	Intents.FLAGS.GUILD_VOICE_STATES
] });

client.once('ready', () => {
	console.log('Ready!');
});

process.stdin.setRawMode(true);
process.stdin.resume();

/*
* Method not needed as of right now but leaving in so there is an example of posting a message to a specific channel.
*/
function playMusic(){
	console.log('sstatatataarrrrt');
	client.channels.fetch('830112738582724618')
		.then(channel => channel.send('/play'))
		.catch(console.error);
}

async function connectToChannel(channel) {
	const connection = joinVoiceChannel({
		channelId: channel.id,
		guildId: channel.guild.id,
		adapterCreator: channel.guild.voiceAdapterCreator,
	});
	try {
		return connection;
	} catch (error) {
		connection.destroy();
		throw error;
	}
}

const player = createAudioPlayer({
	behaviors: {
		noSubscriber: NoSubscriberBehavior.Pause,
	},
});

async function playSong(resource) {
	player.play(resource);
}

class SongQueue {
	constructor() {
		this.queue = [];
		this.isPlaying = false;
	}

	addSong(id) {
		this.queue.push(id);
	}

	playNext() {
		const id = this.queue.shift();
		const filename = `./downloaded_audio/${id}.mp3`;
		const resource = createAudioResource(filename);
		playSong(resource);
		this.isPlaying = true;
	}

	songsInQueue() {
		return this.queue.length;
	}
}

const queue = new SongQueue();

player.on('stateChange', (oldState, newState) => {
	console.log(`Audio player transitioned from ${oldState.status} to ${newState.status}`);
	if (newState.status == 'idle') {
		if (queue.songsInQueue() > 0) {
			queue.playNext();
		} else {
			queue.isPlaying = false;
		}
	}
});

let connections = [];
client.on('messageCreate', async (message) => {
	if (!message.guild) {
		return
	};

	let messageParts = message.content.split(' ');
	if (messageParts[0] === '-join') {
		const channel = message.member?.voice.channel;

		if (channel) {
			message.reply('Joining channel.');
			console.log(`Joining channel ${channel.id}`);
			try {
				const connection = await connectToChannel(channel);
				connection.subscribe(player);
				connections.push(connection);
			} catch (error) {
				console.error(error);
			}
		} else {
			message.reply('Join a voice channel then try again!');
		}
	} else if (messageParts[0] === '-play') {
		console.log('Recieved command to play music.');

		exec(`youtube-dl -f 'bestaudio[ext=m4a]' '${messageParts[1]}' -x --audio-format mp3 -o  './downloaded_audio/%(id)s.mp3'`, async (error, stdout, stderr) => {
			if (error) {
					console.log(`Error downloading audio: ${error.message}`);
					return;
			}

			if (stderr) {
					console.log(`Error downloading audio: ${stderr}`);
					return;
			}

			const matches = stdout.match(/\.\/downloaded_audio\/[^.]+\.mp3/);
			if (matches.length < 1) {
				console.error('No audio file found.');
				return;
			}
			const audioFile = matches[0];
			const youtubeId = audioFile.split('/')[2].split('.')[0];
			queue.addSong(youtubeId);
			if (queue.songsInQueue() === 1 && !queue.isPlaying) {
				message.reply('Playing now!');
				queue.playNext();
			} else {
				message.reply('Added to queue.');
			}
			console.log(`stdout: ${stdout}`);
		});
	}
});

client.login(token);
